//
//  ProductCell.swift
//  Hermitage wool products
//
//  Created by Amit on 23/04/19.
//  Copyright © 2019 Yogesh. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

}
