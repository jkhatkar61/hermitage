//
//  ProductDetail.swift
//  Hermitage wool products
//
//  Created by Amit on 24/04/19.
//  Copyright © 2019 Yogesh. All rights reserved.
//

import UIKit
import MaterialComponents
import FirebaseFirestore
import FirebaseAuth

struct cart {
    var name:String
    var qty:String
}

class ProductDetail: UIViewController {
    
    var db: Firestore!
    var name = ""
     var desc = ""
     var specs = ""
     var image = ""
     var price = ""

    @IBOutlet weak var productimage: UIImageView!
    @IBOutlet weak var productname: UILabel!
    
    @IBOutlet weak var productspec: UILabel!
    @IBOutlet weak var productdesc: UILabel!
    @IBOutlet weak var productprice: UILabel!
    @IBOutlet weak var addtoCart: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
           db = Firestore.firestore()
        self.productdesc?.text = desc
          self.productdesc?.text = desc
          self.productspec?.text = specs
          self.productprice?.text = price
          self.productname?.text = name
        let url = URL(string: image)
//        self.productimage.sd_setImage(with: url, completed: nil)
        
         navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cart", style: .plain, target: self, action: #selector(goToCart))
    }
    
    @objc func goToCart (){
       navigationController?.pushViewController(Cart(), animated: true)
    }
    
    @IBAction func onAddCart(_ sender: Any) {
        let uid = Auth.auth().currentUser?.uid;
        let docData: [String: Any] = [
            "cart": FieldValue.arrayUnion([[
                "name": self.name,
                "qty": 1
                ]])
        ]
        db.collection("users").document(uid ?? "23872").setData(docData, merge: true)
        let message = MDCSnackbarMessage()
        message.text = "Add this item to the cart"
        MDCSnackbarManager.show(message)
    }


}
