//
//  Sendgift.swift
//  Hermitage wool products
//
//  Created by Amit on 24/04/19.
//  Copyright © 2019 Yogesh. All rights reserved.
//

import UIKit
import MaterialComponents

class Sendgift: UIViewController {

    @IBOutlet weak var sendgift: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToCart(tapGestureRecognizer:)))
        sendgift.isUserInteractionEnabled = true
        sendgift.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
    }
    
    @objc func goToCart(tapGestureRecognizer: UITapGestureRecognizer){
        let message = MDCSnackbarMessage()
        message.text = "Added this item to the cart"
        MDCSnackbarManager.show(message)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
