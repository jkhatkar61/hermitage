////
////  Products.swift
////  Hermitage wool products
////
////  Created by Amit on 21/09/18.
////  Copyright © 2018 Yogesh. All rights reserved.
////
//
//import UIKit
//
//class Products: UIViewController,UITableViewDataSource,UITableViewDelegate {
//    
//    var Data = [#imageLiteral(resourceName: "firstproduct"),#imageLiteral(resourceName: "secondproduct"),#imageLiteral(resourceName: "thirdproduct"),#imageLiteral(resourceName: "fourthproduct"),#imageLiteral(resourceName: "fifthproduct")]
//    
//    
//    
//    @IBOutlet weak var tableview: UITableView!
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        tableview.delegate = self
//        tableview.dataSource = self
//        tableview.tableFooterView = UIView()
//        tableview.backgroundColor = UIColor(red:0.77, green:0.78, blue:0.78, alpha:1.0)
//        
//    }
//    //passing the data on click
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        tableview.reloadData()
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return Data.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = Bundle.main.loadNibNamed("Product", owner: self, options: nil)?.first as! Product
//        cell.productimage.image = Data[indexPath.row]
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 335
//    }
//}
//
