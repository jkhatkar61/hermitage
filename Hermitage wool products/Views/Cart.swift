//
//  Cart.swift
//  Hermitage wool products
//
//  Created by Amit on 24/04/19.
//  Copyright © 2019 Yogesh. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

struct dataModel: Decodable {
    var name: String
}

class Cart: UITableViewController {
    
    var data: [dataModel]!
//    var dataMod = [dataModel]!
     var db: Firestore!

    override func viewDidLoad() {
        super.viewDidLoad()
         db = Firestore.firestore()
        fetchData()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    func fetchData() {
        let uid = Auth.auth().currentUser?.uid
        db.collection("users").document(uid ?? "default").getDocument { (document, error) in
            if let document = document, document.exists {
           
                
                let doc  = document.data()
                let cart =  doc!["cart"] as! NSArray
                do {
                    
                    //Convert to Data
                    let jsonData = try JSONSerialization.data(withJSONObject: cart, options: JSONSerialization.WritingOptions.prettyPrinted)
                    
                    //Convert back to string. Usually only do this for debugging
                    if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                        print("S", JSONString)
                        var json = try JSONDecoder().decode([dataModel].self, from: jsonData)
                        
                      self.data = json
                    }
  
                } catch {
                    print(error)
                }

                self.tableView?.reloadData()
    
                
            } else {
                print("Document does not exist")
            }
        }

    }


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                return 45
            }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = Bundle.main.loadNibNamed("Product", owner: self, options: nil)?.first as! Product
        
      
        cell.name.text = data[indexPath.row].name

        return cell
    }
 


}

