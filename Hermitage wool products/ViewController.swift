//
//  ViewController.swift
//  Hermitage wool products
//
//  Created by Amit on 21/09/18.
//  Copyright © 2018 Yogesh. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      print("sd")
        if((Auth.auth().currentUser) != nil){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Dashboard") as! UINavigationController
            self.show(controller, sender:self)
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

