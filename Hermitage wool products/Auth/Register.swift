//
//  Register.swift
//  Hermitage wool products
//
//  Created by Amit on 21/09/18.
//  Copyright © 2018 Yogesh. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import MaterialComponents

class Register: UIViewController {
    let db = Firestore.firestore()
    

    @IBOutlet weak var fname: UITextField!
    @IBOutlet weak var lname: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var mobnumber: UITextField!
    @IBOutlet weak var password: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func validateEntry() -> Bool {
        var isCorrect = true;
        
        if (self.fname.text == "" ){
            isCorrect = false
             let message = MDCSnackbarMessage()
            message.text = "First Name must not be empty"
             MDCSnackbarManager.show(message)
        }else if (self.lname.text == ""){
            isCorrect = false
            let message = MDCSnackbarMessage()
            message.text = "Last Name must not be empty"
            MDCSnackbarManager.show(message)
        }else if (self.email.text == ""){
            isCorrect = false
            let message = MDCSnackbarMessage()
            message.text = "Email must not be empty"
            MDCSnackbarManager.show(message)
        }else if (self.mobnumber.text == ""){
            isCorrect = false
            let message = MDCSnackbarMessage()
            message.text = "Mobile Number must not be empty"
            MDCSnackbarManager.show(message)
        }else if (self.password.text == ""){
            isCorrect = false
            let message = MDCSnackbarMessage()
            message.text = "Password must not be empty"
            MDCSnackbarManager.show(message)
        }
        
        
        return isCorrect
    }

    @IBAction func RegisterButtonClick(_ sender: UIButton) {
        if (self.validateEntry()){
        Auth.auth().createUser(withEmail: email.text!, password: password.text!) { (user, error) in
        
            if error != nil {
                if let errorCode = AuthErrorCode(rawValue: (error?._code)!) {
                    let message = MDCSnackbarMessage()
                    message.text = errorCode.errorMessage
                    MDCSnackbarManager.show(message)
                    print(errorCode)
                }
            }else{
                if user != nil {
                    var ref: DocumentReference? = nil
                    self.db.collection("users").document(user?.user.uid ?? "default").setData([
                        "first": self.fname.text ?? "",
                        "last": self.lname.text ?? "",
                        "email": self.email.text ?? "",
                        "phone" : self.mobnumber.text ?? "",
                        ], merge: true) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "Dashboard") as! UINavigationController
                            self.show(controller, sender:self)
                            print("User Logged In Successfully Edit profile here.")
                        }
                    }
                   
                    
                }
            }
            
        }
        }
    }
    
}





// extension for error messages of Login & Register firebase

extension AuthErrorCode {
    var errorMessage: String {
        switch self {
        case .emailAlreadyInUse:
            return "The email is already in use with another account"
        case .userDisabled:
            return "Your account has been disabled. Please contact support."
        case .invalidEmail:
            return "Please enter a valid email"
        case .networkError:
            return "Network error. Please try again."
        case .weakPassword:
            return "Your password is too weak"
        case .userNotFound:
            return "User not found"
        case .invalidPhoneNumber:
            return "Phone Number is not valid"
        case .wrongPassword:
            return "Your Password is Incorrect"
        case .accountExistsWithDifferentCredential:
            return "Account exist with different Auth Provider"
        case .providerAlreadyLinked:
            return "You can link this only one time."
        default:
            return "Unknown error occurred"
        }
    }
}
