//
//  Login.swift
//  Hermitage wool products
//
//  Created by Amit on 21/09/18.
//  Copyright © 2018 Yogesh. All rights reserved.
//

import UIKit
import FirebaseAuth
import MaterialComponents

class Login: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func validateEntry() -> Bool {
        var isCorrect = true;
        
        if (self.email.text == ""){
            isCorrect = false
            let message = MDCSnackbarMessage()
            message.text = "Email must not be empty"
            MDCSnackbarManager.show(message)
        }else if (self.password.text == ""){
            isCorrect = false
            let message = MDCSnackbarMessage()
            message.text = "Password must not be empty"
            MDCSnackbarManager.show(message)
        }
        
        
        return isCorrect
    }

    @IBAction func LoginButton(_ sender: Any) {
         if (self.validateEntry()){
        Auth.auth().signIn(withEmail: email.text!, password: password.text!) { (user, error) in
            if error != nil {
                if let errorCode = AuthErrorCode(rawValue: (error?._code)!) {
                    let message = MDCSnackbarMessage()
                    message.text = errorCode.errorMessage
                    MDCSnackbarManager.show(message)
                }
                
            }else{
                if user != nil {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "Dashboard") as! UINavigationController
                    self.show(controller, sender:self)
                    print("User Logged In Successfully")
                }
            }
            }
            
        }
    }
    

}
