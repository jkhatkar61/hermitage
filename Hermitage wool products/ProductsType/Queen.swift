//
//  Products.swift
//  Hermitage wool products
//
//  Created by Amit on 21/09/18.
//  Copyright © 2018 Yogesh. All rights reserved.
//

import UIKit
import FirebaseFirestore
import SDWebImage

public struct queenModel {
    var name:String
    var price: String
    var desc: String
    var image: String
    var specs: String
    var dictionary: [String: Any] {
        return [
            "name": name,
            "price": price,
            "desc": desc,
            "image": image,
            "specs": specs,
        ]
    }
}

class Queen: UICollectionViewController {
    var Data = [#imageLiteral(resourceName: "firstproduct"),#imageLiteral(resourceName: "secondproduct"),#imageLiteral(resourceName: "thirdproduct"),#imageLiteral(resourceName: "fourthproduct"),#imageLiteral(resourceName: "fifthproduct")]
    var estimatWidth = 160
    var cellMarginSize = 8.0
    var db: Firestore!
    
    var productmodel: [queenModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "productcell")
        
        fetchData()
    }
    
    
    func fetchData() {
        db.collection("products").whereField("type", isEqualTo: "single").getDocuments { (querySnapshot, error) in
            if error != nil {
                print(error)
            }
            
            
            let results = querySnapshot?.documents.map { (document) -> queenModel in
                if let task = queenModel(dictionary: document.data()){
                    return task
                } else {
                    fatalError("Unable to initialize type \(queenModel.self) with dictionary \(document.data())")
                }
            }
            
            self.productmodel = results
            self.collectionView?.reloadData()
            
            
            
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productmodel?.count ?? 0
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productcell", for: indexPath) as! ProductCell
        let imageurl = URL(string: productmodel[indexPath.row].image)
        cell.productImage.sd_setImage(with: imageurl, completed: nil)
        cell.productName.text = "Product"
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = storyboard.instantiateViewController(withIdentifier: "productdetail") as! ProductDetail
        let data = productmodel[indexPath.row]
        vc1.name = data.name
        vc1.image = data.image
        vc1.desc = data.desc
        vc1.price = "Price: $\(data.price)"
        vc1.specs = data.specs
        navigationController?.pushViewController(vc1, animated: true)
    }
    
    
}



extension Queen: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calulateWidth()
        return CGSize(width: width, height: width)
    }
    
    func calulateWidth() -> CGFloat {
        let estimatedWidth = CGFloat(estimatWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width) / estimatedWidth)
        
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        
        return width
    }
}

extension queenModel{
    init?(dictionary: [String : Any]) {
        guard      let name = dictionary["name"] as? String,
            let price = dictionary["price"] as? String,
            let desc = dictionary["desc"] as? String,
            let specs = dictionary["specs"] as? String,
            let image = dictionary["photo"] as? String
            else { return nil }
        
        self.init(name: name, price: price, desc: desc,image: image, specs: specs)
    }
}
