//
//  TabBar.swift
//  Hermitage wool products
//
//  Created by Amit on 22/04/19.
//  Copyright © 2019 Yogesh. All rights reserved.
//

import UIKit
import Tabman
import Pageboy

class TabBar: TabmanViewController {
    let tabTitles = ["Single", "Double", "Queen", "All"]
    private var viewControllers = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let homeController = Single(collectionViewLayout: UICollectionViewFlowLayout())
        let DoubleController = Double(collectionViewLayout: UICollectionViewFlowLayout())
        let QueenController = Queen(collectionViewLayout: UICollectionViewFlowLayout())
        let AllController = All(collectionViewLayout: UICollectionViewFlowLayout())
        viewControllers = [homeController, DoubleController,QueenController, AllController]
        dataSource = self
        
        // Create bar
        let bar = TMBar.ButtonBar()
        bar.backgroundView.style = .blur(style: .extraLight)
        bar.layout.contentMode = .fit
        bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
        bar.layout.transitionStyle = .snap // Customize
        
        // Add to view
        addBar(bar, dataSource: self , at: .top)
    }
}



extension TabBar: PageboyViewControllerDataSource, TMBarDataSource {
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        
        let title = tabTitles[index]
        return TMBarItem(title: title)
    }
    
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        return viewControllers.count
    }
    
    func viewController(for pageboyViewController: PageboyViewController,
                        at index: PageboyViewController.PageIndex) -> UIViewController? {
        return viewControllers[index]
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        return nil
    }
    
    func barItem(for tabViewController: TabmanViewController, at index: Int) -> TMBarItemable {
        let title = "Page \(index)"
        return TMBarItem(title: title)
    }
}
